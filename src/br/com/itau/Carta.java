package br.com.itau;

public class Carta {
    private Numero numero;
    private Naipe naipe;

    public Carta(Numero numero, Naipe naipe) {
        this.naipe = naipe;
        this.numero = numero;
    }

    public Numero getNumero() {
        return numero;
    }

    public void setNumero(Numero numero) {
        this.numero = numero;
    }

    public Naipe getNaipe() {
        return naipe;
    }

    public void setNaipe(Naipe naipe) {
        this.naipe = naipe;
    }

    @Override
    public String toString() {
        return ("[" + numero + "-" + naipe +  "]");
    }
}

