package br.com.itau;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

public class Mesa {
    private List<Carta> cartas;

    public Mesa() {
        this.cartas = new ArrayList<>();
    }

    public List<Carta> getCartas() {
        return cartas;
    }

    public void setCartas(List<Carta> cartas) {
        this.cartas = cartas;
    }

    public void adicionarCarta(Carta carta) {
        this.cartas.add(carta);
    }

    public String listarCartas() {
        return "Cartas na mesa: { " + cartas + " }";
    }

    public int somaCartas() {
        int soma = 0;
        for (Carta carta : cartas) {
            soma += carta.getNumero().getValor();
        }
        return soma;
    }
}
