package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Baralho {

    private List<Carta> cartas;
    private Random random = new Random();


    public Baralho() {
        this.cartas = new ArrayList<>();
        for (Naipe naipe : Naipe.values()) {
            for (Numero numero : Numero.values()) {
                Carta carta = new Carta(numero, naipe);
                cartas.add(carta);
            }
        }
    }

    public List<Carta> getCartas() {
        return cartas;
    }

    public void setCartas(List<Carta> cartas) {
        this.cartas = cartas;
    }

    public Carta pegarCarta() {
        int indice = random.nextInt(this.cartas.size());
        Carta carta = cartas.get(indice);
        cartas.remove(indice);

        return carta;
    }

}
