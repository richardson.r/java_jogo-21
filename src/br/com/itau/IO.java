package br.com.itau;

import java.util.Scanner;

public class IO {
    public static void inicio() {
        System.out.println("***************************************");
        System.out.println("* * * * * * * * Jogo 21 * * * * * * * *");
        System.out.println("***************************************\n");

        System.out.println("Precione qualquer tecla para iniciar");
        scanner().nextLine();

        jogar();
    }

    private static void jogar() {
        Baralho baralho = new Baralho();
        Mesa mesa = new Mesa();

        String parar = "";
        int soma = 0;
        while (!parar.equals("p")) {
            mesa.adicionarCarta(baralho.pegarCarta());
            soma = mesa.somaCartas();

            System.out.println(mesa.listarCartas());
            System.out.println("Soma: " + soma + "\n");

            if (soma < 21) {
                System.out.println("Pressione 'p' para parar ou qualquer outra tecla para continuar.");
                parar = scanner().nextLine();
            } else break;
        }

        if (soma > 21)
            System.out.println("Não foi dessa vez... Você ultrapassou o valor de 21.");
        else if (soma == 21) {
            System.out.println("***************************************");
            System.out.println("* * * * Parabéns você venceu!!! * * * *");
            System.out.println("***************************************");
        } else
            System.out.println("\nVocê finalizou o jogo com " + soma + " pontos.");


        System.out.print("\nDeseja jogar novamente? (s/n)");
        if (scanner().nextLine().equals("s"))
            inicio();
    }

    private static Scanner scanner() {
        return new Scanner(System.in);
    }
}
