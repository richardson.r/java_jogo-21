package br.com.itau;

public enum Naipe {
    PAUS,
    ESPADA,
    COPAS,
    OUROS;
}
